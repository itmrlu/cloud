const glob = require("glob");
const path = require("path");
const _ = require("lodash");
const model = require("./model");
const redis = require("./redis");
const config = require("./config");

// 全局
global.BaaS = {};
global.BaaS.bookshelf = model.bookshelf;
global.BaaS.Models = model.Models;
global.BaaS.redis = redis;

/**
 * 任务初始化
 */
(async () => {
  // 加载初始化
  const inits = glob.sync("**/*.js", { cwd: "./init" });
  for (const key in inits) {
    if (!_.includes(config.init, inits[key])) {
      config.init.push(inits[key]);
    }
  }
  // 初始化排序
  for (const key in config.init) {
    const init = require(path.resolve("./init", config.init[key]));
    if (_.isFunction(init)) {
      await init();
    }
  }
})();
