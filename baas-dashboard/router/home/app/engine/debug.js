const router = require("koa-router")();
/**
 * api {get} /home/app/debug/list 调试列表
 *
 * apiParam {Number} baas_id 应用id
 *
 */
router.get("/debug", async (ctx, next) => {
  const user = ctx.user;
  const baas = ctx.baas;
  const page = ctx.query.page < 0 ? 0 : parseInt(ctx.query.page);
  if (!user.id) {
    ctx.error("参数错误");
    return;
  }
  const userBaas = await BaaS.Models.user_baas
    .query({ where: { baas_id: baas.id } })
    .fetch();
  // 查询用户应用下的调试
  const debug = await BaaS.Models.baas_debug
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
    })
    .fetchPage({
      pageSize: ctx.config.pageSize,
      page: page,
      withRelated: []
    });
  // 是否开启云函数
  Object.assign(debug, { engine: userBaas.engine });
  ctx.success(debug);
});

module.exports = router;
