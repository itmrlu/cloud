/** 字段**/
const router = require("koa-router")();
/**
 * api {get} /home/app/auth/class/field 字段列表
 *
 *
 */
router.get("/class/field", async (ctx, nex) => {
  const user = ctx.user;
  const baas = ctx.baas;

  const field = await BaaS.Models.class_field
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.orderBy("id", "desc");
    })
    .fetchAll({
      withRelated: ["class"]
    });

  ctx.success(field, "字段列表");
});
/**
 * api {get} /home/app/auth/info/class/field 字段额外信息
 *
 *
 */
router.get("/info/class/field", async (ctx, nex) => {
  const user = ctx.user;
  const baas = ctx.baas;

  // 密钥
  const auth = await BaaS.Models.auth
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.orderBy("id", "desc");
    })
    .fetchAll({
      withRelated: ["class_table"]
    });

  // 分组
  const group = await BaaS.Models.class
    .query(qb => {
      qb.where("baas_id", "=", baas.id);
      qb.orderBy("id", "desc");
    })
    .fetchAll({
      withRelated: []
    });
  // 数据表
  const table = await ctx.table(baas.database); // 数据表

  const data = {
    auth: auth,
    class: group,
    table: table,
    type: ["md5", "bcrypt", "uuid", "uuid.v1", "uuid.v4", "uuid.v5", "moment"]
  };

  ctx.success(data, "字段额外信息");
});
/**
 * api {get} /home/app/auth/class/field/fields/:tableName 查询表字段
 *
 *
 */
router.get("/class/field/fields/:tableName", async (ctx, nex) => {
  const baas = ctx.baas;
  const { tableName } = ctx.params;

  const sql =
    "show full fields from `" + baas.database + "`.`" + tableName + "`";
  const field = await ctx.bookshelf.knex.raw(sql);

  ctx.success(field[0], "数据表字段");
});
/**
 * api {get} /home/app/auth/class/field/:id 获取单个字段
 *
 * apiParam {Number} id 分组id
 *
 */
router.get("/class/field/:id", async (ctx, nex) => {
  const baas = ctx.baas;
  const { id } = ctx.params;

  const field = await BaaS.Models.class_field
    .query(qb => {
      qb.where("id", "=", id);
      qb.where("baas_id", "=", baas.id);
    })
    .fetch();

  ctx.success(field, "单个字段信息");
});
/**
 * api {post} /home/app/auth/add/class/field 新增修改字段
 * 
 * apiParam {
 	id: id,
	class_id: 1,
	table: order,
	name: user_id,
	value: Authorization.id,
	type: md5,
	required: 是否必须1、0,
	url: 请求链接
 } 
 * 
 */
router.post("/add/class/field", async (ctx, nex) => {
  const baas = ctx.baas;
  const classId = ctx.post.class_id;
  const { id, table, name, value, type, required, url } = ctx.post;

  const result = await BaaS.Models.class_field
    .forge({
      id: id,
      baas_id: baas.id,
      class_id: classId,
      table: table,
      name: name,
      value: value,
      type: type,
      required: required,
      url: url
    })
    .save();

  // 删除redis缓存
  await BaaS.redis.delAll(
    `baas:*:appid:${baas.appid}:appkey:${baas.appkey}:*:classFields`
  );
  // 删除redis缓存
  await BaaS.redis.delAll(
    `baas:*:appid:${baas.appid}:appkey:${baas.appkey}:*:table:${table}:*`
  );

  ctx.success(result, "保存成功");
});
/**
 * api {get} /home/app/auth/del/class/field/:id 删除字段
 *
 * apiParam {Number} id
 *
 */
router.get("/del/class/field/:id", async (ctx, nex) => {
  const baas = ctx.baas;
  const { id } = ctx.params;

  const classField = await BaaS.Models.class_field
    .query(qb => {
      qb.where("id", "=", id);
      qb.where("baas_id", "=", baas.id);
    })
    .fetch();
  if (!classField) {
    ctx.error("", "参数错误");
    return;
  }

  const result = await BaaS.Models.class_field
    .forge({
      id: id
    })
    .destroy();

  // 删除redis缓存
  await BaaS.redis.delAll(
    `baas:*:appid:${baas.appid}:appkey:${baas.appkey}:*:classFields`
  );
  // 删除redis缓存
  await BaaS.redis.delAll(
    `baas:*:appid:${baas.appid}:appkey:${baas.appkey}:*:table:${
      classField.table
    }:*`
  );

  ctx.success(result, "删除字段");
});

module.exports = router;
