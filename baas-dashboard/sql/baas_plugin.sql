# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.16-MariaDB)
# Database: baas_plugin
# Generation Time: 2017-11-29 14:32:29 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE `baas2_plugin` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `baas2_plugin`;

# Dump of table captcha_img
# ------------------------------------------------------------

DROP TABLE IF EXISTS `captcha_img`;

CREATE TABLE `captcha_img` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(10) DEFAULT NULL,
  `length` int(10) NOT NULL DEFAULT '0' COMMENT '验证码长度',
  `exclude` text NOT NULL COMMENT '字符排除',
  `line` int(10) NOT NULL DEFAULT '0' COMMENT '干扰线条数量',
  `color` tinyint(2) NOT NULL DEFAULT '0' COMMENT '字符是否有颜色',
  `bgcolor` text COMMENT '验证码图片背景色',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `captcha_img` WRITE;
/*!40000 ALTER TABLE `captcha_img` DISABLE KEYS */;

INSERT INTO `captcha_img` (`id`, `baas_id`, `length`, `exclude`, `line`, `color`, `bgcolor`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,11,6,'2',1,1,'#06A0FD','2017-11-28 18:59:05','2017-11-28 18:59:05',NULL),
	(2,3,2,'',0,1,'#06A0FD','2017-11-28 19:26:34','2017-11-28 19:26:34',NULL),
	(3,3,3,'',0,0,'#fff','2017-11-28 19:27:12','2017-11-28 19:27:12',NULL),
	(4,3,5,'',0,0,'#36EB90','2017-11-28 19:27:27','2017-11-28 19:27:27',NULL),
	(5,1,6,'0oli1',2,0,'#fff','2017-11-28 20:17:01','2017-11-28 20:17:01',NULL),
	(6,4,32,'字',10,0,'#fff','2017-11-28 20:27:50','2017-11-28 20:27:50',NULL),
	(7,6,2,'',0,0,'#fff','2017-11-29 09:45:29','2017-11-29 09:45:29',NULL),
	(8,31,0,'qwerqwer',0,0,'#3BFA9A','2017-11-29 11:14:40','2017-11-29 11:14:40',NULL),
	(9,6,1,'',0,0,'#fff','2017-11-29 14:00:34','2017-11-29 14:00:34',NULL),
	(10,6,1,'',0,0,'#fff','2017-11-29 14:00:37','2017-11-29 14:00:37',NULL),
	(11,31,0,'ertyrt',0,0,'#fff','2017-11-29 14:01:11','2017-11-29 14:01:11',NULL),
	(12,31,123,'www',2,0,'#fff','2017-11-29 14:01:31','2017-11-29 14:01:31',NULL),
	(13,31,6,'www',2,0,'#fff','2017-11-29 14:01:55','2017-11-29 14:01:55',NULL),
	(14,31,6,'4',4,0,'#fff','2017-11-29 14:03:46','2017-11-29 14:03:46',NULL),
	(15,31,6,'4',4,0,'#fff','2017-11-29 14:04:07','2017-11-29 14:04:07',NULL),
	(16,31,6,'t',3,0,'#fff','2017-11-29 14:05:04','2017-11-29 14:05:04',NULL),
	(17,12,4,'',3,1,'#fff','2017-11-29 15:04:58','2017-11-29 15:04:58',NULL),
	(18,6,1,'',0,0,'#fff','2017-11-29 16:02:45','2017-11-29 16:02:45',NULL),
	(19,6,2,'32',23,0,'#fff','2017-11-29 16:03:49','2017-11-29 16:03:49',NULL),
	(20,2,1,'',0,0,'#fff','2017-11-29 17:14:13','2017-11-29 17:14:13',NULL),
	(21,2,1,'1',1,0,'#fff','2017-11-29 17:14:33','2017-11-29 17:14:33',NULL),
	(22,2,1,'1',1,0,'#fff','2017-11-29 17:14:54','2017-11-29 17:14:54',NULL),
	(23,2,1,'1',1,1,'#fff','2017-11-29 17:15:15','2017-11-29 17:15:15',NULL),
	(24,2,1,'1',1,1,'#fff','2017-11-29 17:15:24','2017-11-29 17:15:24',NULL),
	(25,1,111,'111',111,0,'#fff','2017-11-29 17:17:55','2017-11-29 17:17:55',NULL),
	(26,2,1,'1',1,1,'#17CD72','2017-11-29 17:18:17','2017-11-29 17:18:17',NULL),
	(27,2,2,'2',2,1,'#2AF690','2017-11-29 17:18:27','2017-11-29 17:18:27',NULL),
	(28,2,2,'2',2,0,'#fff','2017-11-29 17:19:40','2017-11-29 17:19:40',NULL),
	(29,12,5,'',0,0,'#fff','2017-11-29 17:40:27','2017-11-29 17:40:27',NULL),
	(30,3,6,'#%$!^&*()`',15,1,'#A5F3CC','2017-11-29 19:31:27','2017-11-29 19:31:27',NULL),
	(31,3,4,'',20,1,'#88AEEC','2017-11-29 19:32:45','2017-11-29 19:32:45',NULL);

/*!40000 ALTER TABLE `captcha_img` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table captcha_sms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `captcha_sms`;

CREATE TABLE `captcha_sms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(10) NOT NULL DEFAULT '0',
  `name` text COMMENT '模板名称',
  `content` text COMMENT '模板内容',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `captcha_sms` WRITE;
/*!40000 ALTER TABLE `captcha_sms` DISABLE KEYS */;

INSERT INTO `captcha_sms` (`id`, `baas_id`, `name`, `content`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,6,'1','1','2017-11-28 15:02:30','2017-11-28 15:02:30',NULL),
	(2,11,'呵呵','取消','2017-11-28 18:58:34','2017-11-28 18:58:34',NULL),
	(3,3,'1','qingful','2017-11-28 19:23:33','2017-11-28 19:23:33',NULL),
	(4,1,'验证码','您的验证码，是${code}','2017-11-28 20:10:30','2017-11-28 20:10:30',NULL),
	(5,6,'1','1','2017-11-29 09:44:48','2017-11-29 09:44:48',NULL),
	(6,6,'1','1','2017-11-29 09:44:48','2017-11-29 09:44:48',NULL),
	(7,6,'1','1','2017-11-29 09:44:48','2017-11-29 09:44:48',NULL),
	(8,3,'1','1','2017-11-29 10:02:14','2017-11-29 10:02:14',NULL),
	(9,3,'2','验证码：   （10分钟内有效），请及时完成验证','2017-11-29 10:04:21','2017-11-29 10:04:21',NULL),
	(10,31,'qq','qwerqwer','2017-11-29 11:14:18','2017-11-29 11:14:18',NULL),
	(11,6,'1','1','2017-11-29 14:00:28','2017-11-29 14:00:28',NULL),
	(12,31,'444','retwert','2017-11-29 14:04:24','2017-11-29 14:04:24',NULL),
	(13,12,'修改密码','验证码','2017-11-29 15:04:34','2017-11-29 15:04:34',NULL),
	(14,3,'3','3333','2017-11-29 16:26:31','2017-11-29 16:26:31',NULL),
	(15,3,'3','3333','2017-11-29 16:26:32','2017-11-29 16:26:32',NULL),
	(16,3,'4','44','2017-11-29 16:28:57','2017-11-29 16:28:57',NULL),
	(17,3,'4','44','2017-11-29 16:28:57','2017-11-29 16:28:57',NULL),
	(18,3,'5','555','2017-11-29 16:35:45','2017-11-29 16:35:45',NULL),
	(19,3,'5','555','2017-11-29 16:35:45','2017-11-29 16:35:45',NULL),
	(20,3,'6','6','2017-11-29 16:36:10','2017-11-29 16:36:10',NULL),
	(21,2,'1','111','2017-11-29 17:13:01','2017-11-29 17:13:01',NULL),
	(22,12,'注册账号','验证码','2017-11-29 17:39:24','2017-11-29 17:39:24',NULL),
	(23,2,'1','655','2017-11-29 17:41:56','2017-11-29 17:41:56',NULL),
	(24,3,'7','7','2017-11-29 19:35:57','2017-11-29 19:35:57',NULL);

/*!40000 ALTER TABLE `captcha_sms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms`;

CREATE TABLE `sms` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `baas_id` int(11) NOT NULL DEFAULT '0',
  `number` int(11) NOT NULL DEFAULT '0' COMMENT '短信条数',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sms` WRITE;
/*!40000 ALTER TABLE `sms` DISABLE KEYS */;

INSERT INTO `sms` (`id`, `baas_id`, `number`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,3,20,'2017-11-29 22:15:50','2017-11-29 22:15:50',NULL),
	(2,38,0,'2017-11-29 22:29:04',NULL,NULL);

/*!40000 ALTER TABLE `sms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table sms_fee
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sms_fee`;

CREATE TABLE `sms_fee` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `price` float(10,2) NOT NULL DEFAULT '0.00',
  `number` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `sms_fee` WRITE;
/*!40000 ALTER TABLE `sms_fee` DISABLE KEYS */;

INSERT INTO `sms_fee` (`id`, `price`, `number`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,10.00,20,NULL,NULL,NULL),
	(2,100.00,210,NULL,NULL,NULL),
	(3,200.00,420,NULL,NULL,NULL);

/*!40000 ALTER TABLE `sms_fee` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
