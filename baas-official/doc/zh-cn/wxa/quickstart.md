# 微信小程序快速开始

##### 功能列表

- 用户信息（获取用户信息、绑定微信用户为小程序体验者、解除绑定小程序的体验者）
- 模版消息（获取小程序模板库标题列表、获取模板库某个模板标题下关键词库、组合模板并添加至帐号下的个人模板库、获取帐号下已存在的模板列表、删除帐号下的某个模板、发送模板消息）
- 客服消息（发送文本消息、发送图片消息、发送图文链接、发送小程序卡片）
- 小程序码（创建临时、永久二维码，查看二维码URL）
- 附近（添加地点、删除地点、展示/取消展示附近小程序）
- 数据分析（概况趋势、访问趋势、访问分布、访问留存、访问页面、用户画像）

示例

```js
const wxa = new module.WxaApi('微信小程序的appid', '微信小程序的appsecret');
```

当多进程时，token需要全局维护，以下为保存token的接口：

```js
const wxa = new module.WxaApi('微信小程序的appid', '微信小程序的appsecret', async function (openid) {
  // 传入一个获取全局token的方法
  return await fse.readJson(openid +':access_token.txt', 'utf8');
}, async function (token) {
  // 请将token存储到全局，跨进程、跨机器级别的全局，比如写到数据库、redis等
  // 这样才能在cluster模式及多机情况下使用，以下为写入到文件的示例
  await fse.outputJson(openid + ':access_token.txt', token);
});
```
