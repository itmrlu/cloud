# 客服消息

------

#### 发送文本消息

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| openid | 是 | 用户的openid |
| text | 是 | 发送的消息内容 |

```js
const sendText = await wxa.sendText( openid, text );
```

#### 发送图片消息

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| openid | 是 | 用户的openid |
| mediaId | 是 | 媒体文件的ID，参见uploadMedia方法 |

```js
const sendImage = await wxa.sendImage( openid, mediaId );
```

#### 发送图文链接

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| openid | 是 | 用户的openid |
| title | 是 | 消息标题 |
| description | 是 | 图文链接消息 |
| url | 是 | 图文链接消息被点击后跳转的链接 |
| thumbUrl | 是 | 封面图片的临时cdn链接 |

```js
const sendLink = await wxa.sendLink( openid, title, description, url, thumbUrl );
```

#### 发送小程序卡片

|参数 | 必填 |  说明 |
| ---- | ------------------ | ---- |
| openid | 是 | 用户的openid |
| title | 是 | 消息标题 |
| pagepath | 是 | 小程序的页面路径，跟app.json对齐，支持参数，比如pages/index/index?foo=bar |
| thumbMediaId | 是 | 小程序消息卡片的封面， image类型的media_id，通过新增素材接口上传图片文件获得，建议大小为520*416 |

```js
const sendCard = await wxa.sendCard( openid, title, pagepath, thumbMediaId );
```